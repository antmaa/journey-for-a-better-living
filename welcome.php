<?php
// Start the session
session_start();
?>
<!DOCTYPE HTML>  
<html>
<head>
<?php
if(stristr($_SERVER['HTTP_USER_AGENT'], "Mobile")){ 
?>
<link rel="stylesheet" href="styleMob2.css" type="text/css" />
<?php
}
else { 
?>
<link rel="stylesheet" href="styleDesk2.css" type="text/css" />
<?php
}
?>
<script>
    //var state points the question which you are reading
    
    var state = 0;
    //var joker is a state when user has answered a question but not yet changet to the next. Joker gives positive feedback for changing your answer
    var joker = false;
    var orgnum = 0;
    var first = true;
    function start(){
        document.getElementById('b1').style.visibility='hidden';
        document.getElementById('f1').style.visibility='visible';
        document.getElementById('text1').style.visibility='hidden';
        document.getElementById('text2').style.visibility='hidden';
        document.getElementById('q1').style.visibility='visible';
        document.getElementById('tree').style.visibility='visible';
    }
    function changeArrow1(){
    document.getElementById('forward').src= "arrow.png";       
    }
    function changeArrow2(){
    document.getElementById('forward').src= "arrow2.png";       
    }
    //function takes a onclick event number and displays a message depending on the chosen radio button.
    function comment(num){
        var number = num;
        var commentId = "comment" + state;
        var com = document.getElementById(commentId);
        com.innerHTML="";
        document.getElementById('forward').style.visibility='visible';
    //check if user is changing aswer and is the user trying to put the riginal aswer
        
        if(joker == true && number != orgnum){
           var randomNum = Math.floor(Math.random() * 3);   
           joker = false;
        
    //give one of 3 aswers
        switch (randomNum){
           case 0:
               var comtext = document.createTextNode("that sounds more like it"); 
               com.appendChild(comtext);
               break;
           case 1:
               var comtext = document.createTextNode("We are making progress"); 
               com.appendChild(comtext);
               break;
           case 2:
               var comtext = document.createTextNode("Hesitating is a way of learning something more deeply"); 
               com.appendChild(comtext);
               break;
                
        }
          return;  
        }
        switch (number){
            case 1:
               var comtext = document.createTextNode("Think: are you a lazy person?"); 
               com.appendChild(comtext);
               joker = true;
               
               break;
            case 2:
                var comtext = document.createTextNode("You don't know?"); 
               com.appendChild(comtext);
                joker = true;
               break;
            case 3:    
               var comtext = document.createTextNode("Never?"); 
               com.appendChild(comtext);
               joker = true;
               break;
            //2nd-----------------------------------------------------
            case 4:    
               var comtext = document.createTextNode("Pat yourself on the back"); 
               com.appendChild(comtext);
               joker = true;
               break;
            case 5:    
               var comtext = document.createTextNode("I think you know better"); 
               com.appendChild(comtext);
               joker = true;
               break;
            case 6:    
               var comtext = document.createTextNode("You think that poorly about yourself?"); 
               com.appendChild(comtext);
               joker = true;
               break;
            //3rd-----------------------------------------------------
            case 7:    
               var comtext = document.createTextNode("Think deeply. Why?"); 
               com.appendChild(comtext);
               joker = true;
               break;
            case 8:    
               var comtext = document.createTextNode("You don't know?"); 
               com.appendChild(comtext);
               joker = true;
               break;
            case 9:    
               var comtext = document.createTextNode("Nice to hear"); 
               com.appendChild(comtext);
               joker = true;
               break;
            //4th-----------------------------------------------------
            case 10:    
               var comtext = document.createTextNode("Not even in the most desperate situation?"); 
               com.appendChild(comtext);
               joker = true;
               break;
            case 11:    
               var comtext = document.createTextNode("The questionare appreciates honesty, don't hesitate"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            case 12:    
               var comtext = document.createTextNode("Think to yourself: why is that?"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            //5th-----------------------------------------------------
            case 13:    
               var comtext = document.createTextNode("Look at you"); 
               com.appendChild(comtext);
               joker = true;
               break;
            case 14:    
               var comtext = document.createTextNode("was the enviroment good or bad?"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            case 15:    
               var comtext = document.createTextNode("not good at all?"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            //6th-----------------------------------------------------
            case 16:    
               var comtext = document.createTextNode("Maybe the problem is betwean your ears"); 
               com.appendChild(comtext);
               joker = true;
               break;
            case 17:    
               var comtext = document.createTextNode("Which one is it?"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            case 18:    
               var comtext = document.createTextNode("Do you think you can read everyone?"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            //7th-----------------------------------------------------
            case 19:    
               var comtext = document.createTextNode("that is quite a generalization"); 
               com.appendChild(comtext);
               joker = true;
               break;
            case 20:    
               var comtext = document.createTextNode("you really don't have an opinion?"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            case 21:    
               var comtext = document.createTextNode("maybe you are a little gullible"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            //8th-----------------------------------------------------
            case 22:    
               var comtext = document.createTextNode("Why do you think that is?"); 
               com.appendChild(comtext);
               joker = true;
               break;
            case 23:    
               var comtext = document.createTextNode("One way or another?"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            case 24:    
               var comtext = document.createTextNode("It's a good thing to know what you want"); 
               com.appendChild(comtext);
               joker = true;
               break;
            //9th-----------------------------------------------------
            case 25:    
               var comtext = document.createTextNode("Well you made a quick change of personality"); 
               com.appendChild(comtext);
               joker = true;
               break;
            case 26:    
               var comtext = document.createTextNode("You really don't have an opinion"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            case 27:    
               var comtext = document.createTextNode("You really think so?"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            //10th-----------------------------------------------------
            case 28:    
               var comtext = document.createTextNode("What is your motivation in the same sitsuation?"); 
               com.appendChild(comtext);
               joker = true;
               break;
            case 29:    
               var comtext = document.createTextNode("are you sure"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            case 30:    
               var comtext = document.createTextNode("do you think yourself as a guillible person"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            //11th-----------------------------------------------------
            case 31:    
               var comtext = document.createTextNode("you NEVER cry?"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            case 32:    
               var comtext = document.createTextNode("so are you just not sure?"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            case 33:    
               var comtext = document.createTextNode("frail person has a frail mind"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            //12th-----------------------------------------------------
            case 34:    
               var comtext = document.createTextNode("Do you think it is always natural?"); 
               com.appendChild(comtext);
               joker = true;
               break;
            case 35:    
               var comtext = document.createTextNode("make your mind"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            case 36:    
               var comtext = document.createTextNode("if it's not natural, what do you think it is?"); 
               com.appendChild(comtext);
               joker = true;
               break;
             //13th-----------------------------------------------------
            case 37:    
               var comtext = document.createTextNode("How much of the unknown do you explain with the higher power?"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            case 38:    
               var comtext = document.createTextNode("Maybe it is time to make yor mind?"); 
               com.appendChild(comtext);
               joker = true;
               break; 
            case 39:    
               var comtext = document.createTextNode("You think you can explain everything?"); 
               com.appendChild(comtext);
               joker = true;
               break;
            //13th-----------------------------------------------------
            case 40:    
               var comtext = document.createTextNode("Like we all"); 
               com.appendChild(comtext);
               joker = true;
               break;
            case 41:    
               var comtext = document.createTextNode("you don't know yourself?"); 
               com.appendChild(comtext);
               joker = true;
               break;
            case 42:    
               var comtext = document.createTextNode("Do you think you answered honestly?"); 
               com.appendChild(comtext);
               joker = true;
               break;
            //SUBMIT-----------------------------------------------------
            case 43: 
               var com = document.getElementById("comment13")
               var comtext = document.createTextNode("Sorry, but your tree withered. Please press submit to get your results."); 
               com.appendChild(comtext);
               break;
        }  
        if(first == true){
        orgnum = number; 
        first = false;
        }
        return;
    }
    //forward button checks the state, hides and makes selected elements visible 
    function changeQFor(){
    switch (state){
        case 0:
        document.getElementById('q1').style.visibility='hidden';
        document.getElementById('q2').style.visibility='visible';
        document.getElementById('forward').style.visibility='hidden';
        document.getElementById('tree').src='tree2.png';   
        state = 1;
        break;
        case 1:
        document.getElementById('q2').style.visibility='hidden';
        document.getElementById('q3').style.visibility='visible';
        document.getElementById('forward').style.visibility='hidden';
        document.getElementById('tree').src='tree3.png'; 
        state = 2;
        break;
        case 2:
        document.getElementById('q3').style.visibility='hidden';
        document.getElementById('q4').style.visibility='visible';
        document.getElementById('forward').style.visibility='hidden';
        document.getElementById('tree').src='tree4.png'; 
        state = 3;
        break;
        case 3:
        document.getElementById('q4').style.visibility='hidden';
        document.getElementById('q5').style.visibility='visible';
        document.getElementById('forward').style.visibility='hidden';
        document.getElementById('tree').src='tree5.png';
        state = 4;
        break;
        case 4:
        document.getElementById('q5').style.visibility='hidden';
        document.getElementById('q6').style.visibility='visible';
        document.getElementById('forward').style.visibility='hidden';
        document.getElementById('tree').src='tree6.png';
        state = 5;
        break;
        case 5:
        document.getElementById('q6').style.visibility='hidden';
        document.getElementById('q7').style.visibility='visible';
        document.getElementById('forward').style.visibility='hidden';
        document.getElementById('tree').src='tree7.png';
        state = 6;
        break;
        case 6:
        document.getElementById('q7').style.visibility='hidden';
        document.getElementById('q8').style.visibility='visible';
        document.getElementById('forward').style.visibility='hidden';
        document.getElementById('tree').src='tree8.png';
        state = 7;
        break;
        case 7:
        document.getElementById('q8').style.visibility='hidden';
        document.getElementById('q9').style.visibility='visible';
        document.getElementById('forward').style.visibility='hidden';
        document.getElementById('tree').src='tree9.png';
        state = 8;
        break;
        case 8:
        document.getElementById('q9').style.visibility='hidden';
        document.getElementById('q10').style.visibility='visible';
        document.getElementById('forward').style.visibility='hidden';
        document.getElementById('tree').src='tree8.png';
        state = 9;
        break;
        case 9:
        document.getElementById('q10').style.visibility='hidden';
        document.getElementById('q11').style.visibility='visible';
        document.getElementById('forward').style.visibility='hidden';
        document.getElementById('tree').src='tree9.png';
        state = 10;
        break;
        case 10:
        document.getElementById('q11').style.visibility='hidden';
        document.getElementById('q12').style.visibility='visible';
        document.getElementById('forward').style.visibility='hidden';
        document.getElementById('tree').src='tree10.png';
        state = 11;
        break;
        case 11:
        document.getElementById('q12').style.visibility='hidden';
        document.getElementById('q13').style.visibility='visible';
        document.getElementById('forward').style.visibility='hidden';
        document.getElementById('tree').src='tree11.png';
        state = 12;
        break;
        case 12:
        document.getElementById('q13').style.visibility='hidden';
        document.getElementById('q14').style.visibility='visible';
        document.getElementById('forward').style.visibility='hidden';
        document.getElementById('tree').src='tree12.png';
        state = 13;
        break;
        case 13:
        document.getElementById('submit').style.visibility='visible';
        first = true;
        joker = false;
        comment(43);
        document.getElementById('forward').style.visibility='hidden';
        break;
        
    }
        //if joker is true it's not anymore. and the boolean for finding first user check is reset to true
        joker = false;
        first = true;
        return;
    }
</script> 
</head>
<body>
<?php
$name1 = $_GET["fname"];
$name2 = $_GET["lname"];
$_SESSION['fname'] = $name1; 
$_SESSION['lname'] = $name2;
?> 
<div class="start">
<h2 id="text1"> Thank you for joining our great cause <?php echo $_GET["fname"]; ?><br></h2>
<p id="text2"> This is a personality test based on a research made by Swiss psychiatrist Bill Suth. In his research, Bill Suth divides the population into 27 different personality types. Based heavily on Sigmund Freud's psychoanalysis, your own personality type can be discovered by answering just a couple of simple questions. By answering these 10-30 precisely selected questions, we can reveal your personality type with great accuracy. By discovering who you really are, you can better your life in countless ways. As the late Bill Suth described personal growth:" When we're born, mentally we are just a mere sappling, but with the help of the work on our inner selves and self discovery, we can grow into a beautiful and mighty tree, the fruits of which can be enjoyed by everyone around you." 
<br>
Now, if you are willing. Press start, plant your seed of life and see just how beautiful your tree will become.
<br> 
<br> 
<button id="b1" class="buttonstart" onClick="start()" style="visibility:visible;"> START!</button>
</div> 
<div>    
<form id="f1" class="Form" method="post"  action="results.php">
<!-- 1-------------------------------------------------------------------------------------------------- -->
<div id="q1" class="question">
Some days I feel like not getting up from my bed.<br>
<!-- options container for anchoring the div to the bottom of div-question -->
<div class="options">
<p class="agree">Agree   </p> 
Neutral
<p class="disagree">Disagree</p>
<br>
<label class="container1" style="float:left;">
<input class="option1" onclick="comment(1)" type="radio" name="q0" value="1">
<span class="checkmark1"></span>
</label>
<label class="container2">
<input  class="option2" onclick="comment(2)"type="radio" name="q0" value="2">
<span class="checkmark2"></span>
</label>
<label class="container3" style="float:right;">
<input  class="option3" onclick="comment(3)"type="radio" name="q0" value="3">
<span class="checkmark3"></span>
</label>
<br>
<br>
<p id="comment0"></p>
</div>
</div> 
<!-- 2-------------------------------------------------------------------------------------------------- -->
<div id="q2" class="question">
I consier myself a good person <br>
<!-- options container for anchoring the div to the bottom of div-question -->
<div class="options">
<p class="agree">Agree   </p> 
Neutral
<p class="disagree">Disagree</p>
<br>
<label class="container1" style="float:left;">
<input class="option1" onclick="comment(4)" type="radio" name="q1" value="1">
<span class="checkmark1"></span>
</label>
<label class="container2">
<input  class="option2" onclick="comment(5)"type="radio" name="q1" value="2">
<span class="checkmark2"></span>
</label>
<label class="container3" style="float:right;">
<input  class="option3" onclick="comment(6)"type="radio" name="q1" value="3">
<span class="checkmark3"></span>
</label>
<br>
<br>
<p id="comment1"></p>
</div>
</div> 
<!-- 3-------------------------------------------------------------------------------------------------- -->

<div id="q3" class="question">
I dislike animals  <br>
<!-- options container for anchoring the div to the bottom of div-question -->
<div class="options">
<p class="agree">Agree   </p> 
Neutral
<p class="disagree">Disagree</p>
<br>
<label class="container1" style="float:left;">
<input class="option1" onclick="comment(7)" type="radio" name="q2" value="1">
<span class="checkmark1"></span>
</label>
<label class="container2">
<input  class="option2" onclick="comment(8)"type="radio" name="q2" value="2">
<span class="checkmark2"></span>
</label>
<label class="container3" style="float:right;">
<input  class="option3" onclick="comment(9)"type="radio" name="q2" value="3">
<span class="checkmark3"></span>
</label>
<br>
<br>
<p id="comment2"></p>
</div>
</div> 
<!-- 4-------------------------------------------------------------------------------------------------- -->
<div id="q4" class="question">
I never could steal anything from another person. <br>
<!-- options container for anchoring the div to the bottom of div-question -->
<div class="options">
<p class="agree">Agree   </p> 
Neutral
<p class="disagree">Disagree</p>
<br>
<label class="container1" style="float:left;">
<input class="option1" onclick="comment(10)" type="radio" name="q3" value="1">
<span class="checkmark1"></span>
</label>
<label class="container2">
<input  class="option2" onclick="comment(11)"type="radio" name="q3" value="2">
<span class="checkmark2"></span>
</label>
<label class="container3" style="float:right;">
<input  class="option3" onclick="comment(12)"type="radio" name="q3" value="3">
<span class="checkmark3"></span>
</label>
<br>
<br>
<p id="comment3"></p>
</div>
</div> 
<!-- 5-------------------------------------------------------------------------------------------------- -->
<div id="q5" class="question">
I was raised in a good enviroment <br>
<!-- options container for anchoring the div to the bottom of div-question -->
<div class="options">
<p class="agree">Agree   </p> 
Neutral
<p class="disagree">Disagree</p>
<br>
<label class="container1" style="float:left;">
<input class="option1" onclick="comment(13)" type="radio" name="q4" value="1">
<span class="checkmark1"></span>
</label>
<label class="container2">
<input  class="option2" onclick="comment(14)"type="radio" name="q4" value="2">
<span class="checkmark2"></span>
</label>
<label class="container3" style="float:right;">
<input  class="option3" onclick="comment(15)"type="radio" name="q4" value="3">
<span class="checkmark3"></span>
</label>
<br>
<br>
<p id="comment4"></p>
</div>
</div>
<!-- 6-------------------------------------------------------------------------------------------------- -->
<div id="q6" class="question">
Sometimes I don't get what people are trying to accomblish with their actions <br>
<!-- options container for anchoring the div to the bottom of div-question -->
<div class="options">
<p class="agree">Agree   </p> 
Neutral
<p class="disagree">Disagree</p>
<br>
<label class="container1" style="float:left;">
<input class="option1" onclick="comment(16)" type="radio" name="q5" value="1">
<span class="checkmark1"></span>
</label>
<label class="container2">
<input  class="option2" onclick="comment(17)"type="radio" name="q5" value="2">
<span class="checkmark2"></span>
</label>
<label class="container3" style="float:right;">
<input  class="option3" onclick="comment(18)"type="radio" name="q5" value="3">
<span class="checkmark3"></span>
</label>
<br>
<br>
<p id="comment5"></p>
</div>
</div>
<!-- 7-------------------------------------------------------------------------------------------------- -->
<div id="q7" class="question">
Generally there are good and there are bad people. <br>
<!-- options container for anchoring the div to the bottom of div-question -->
<div class="options">
<p class="agree">Agree   </p> 
Neutral
<p class="disagree">Disagree</p>
<br>
<label class="container1" style="float:left;">
<input class="option1" onclick="comment(19)" type="radio" name="q6" value="1">
<span class="checkmark1"></span>
</label>
<label class="container2">
<input  class="option2" onclick="comment(20)"type="radio" name="q6" value="2">
<span class="checkmark2"></span>
</label>
<label class="container3" style="float:right;">
<input  class="option3" onclick="comment(21)"type="radio" name="q6" value="3">
<span class="checkmark3"></span>
</label>
<br>
<br>
<p id="comment6"></p>
</div>
</div>
<!-- 8-------------------------------------------------------------------------------------------------- -->
<div id="q8" class="question">
Sometimes I am morbidly jealous <br>
<!-- options container for anchoring the div to the bottom of div-question -->
<div class="options">
<p class="agree">Agree   </p> 
Neutral
<p class="disagree">Disagree</p>
<br>
<label class="container1" style="float:left;">
<input class="option1" onclick="comment(22)" type="radio" name="q7" value="1">
<span class="checkmark1"></span>
</label>
<label class="container2">
<input  class="option2" onclick="comment(23)"type="radio" name="q7" value="2">
<span class="checkmark2"></span>
</label>
<label class="container3" style="float:right;">
<input  class="option3" onclick="comment(24)"type="radio" name="q7" value="3">
<span class="checkmark3"></span>
</label>
<br>
<br>
<p id="comment7"></p>
</div>
</div>
<!-- 9-------------------------------------------------------------------------------------------------- -->
<div id="q9" class="question">
You should treat other people the way you want to be treated yourself <br>
<!-- options container for anchoring the div to the bottom of div-question -->
<div class="options">
<p class="agree">Agree   </p> 
Neutral
<p class="disagree">Disagree</p>
<br>
<label class="container1" style="float:left;">
<input class="option1" onclick="comment(25)" type="radio" name="q8" value="1">
<span class="checkmark1"></span>
</label>
<label class="container2">
<input  class="option2" onclick="comment(26)"type="radio" name="q8" value="2">
<span class="checkmark2"></span>
</label>
<label class="container3" style="float:right;">
<input  class="option3" onclick="comment(27)"type="radio" name="q8" value="3">
<span class="checkmark3"></span>
</label>
<br>
<br>
<p id="comment8"></p>
</div>
</div>
<!-- 10-------------------------------------------------------------------------------------------------- -->
<div id="q10" class="question">
When someone does a good deed for myself they have selfis motives <br>
<!-- options container for anchoring the div to the bottom of div-question -->
<div class="options">
<p class="agree">Agree   </p> 
Neutral
<p class="disagree">Disagree</p>
<br>
<label class="container1" style="float:left;">
<input class="option1" onclick="comment(28)" type="radio" name="q9" value="1">
<span class="checkmark1"></span>
</label>
<label class="container2">
<input  class="option2" onclick="comment(29)"type="radio" name="q9" value="2">
<span class="checkmark2"></span>
</label>
<label class="container3" style="float:right;">
<input  class="option3" onclick="comment(30)"type="radio" name="q9" value="3">
<span class="checkmark3"></span>
</label>
<br>
<br>
<p id="comment9"></p>
</div>
</div>
<!-- 11-------------------------------------------------------------------------------------------------- -->
<div id="q11" class="question">
I never cry. <br>
<!-- options container for anchoring the div to the bottom of div-question -->
<div class="options">
<p class="agree">Agree   </p> 
Neutral
<p class="disagree">Disagree</p>
<br>
<label class="container1" style="float:left;">
<input class="option1" onclick="comment(31)" type="radio" name="q10" value="1">
<span class="checkmark1"></span>
</label>
<label class="container2">
<input  class="option2" onclick="comment(32)"type="radio" name="q10" value="2">
<span class="checkmark2"></span>
</label>
<label class="container3" style="float:right;">
<input  class="option3" onclick="comment(33)"type="radio" name="q10" value="3">
<span class="checkmark3"></span>
</label>
<br>
<br>
<p id="comment10"></p>
</div>
</div>
<!-- 12-------------------------------------------------------------------------------------------------- -->
<div id="q12" class="question">
Death is a natural part of life <br>
<!-- options container for anchoring the div to the bottom of div-question -->
<div class="options">
<p class="agree">Agree   </p> 
Neutral
<p class="disagree">Disagree</p>
<br>
<label class="container1" style="float:left;">
<input class="option1" onclick="comment(34)" type="radio" name="q11" value="1">
<span class="checkmark1"></span>
</label>
<label class="container2">
<input  class="option2" onclick="comment(35)"type="radio" name="q11" value="2">
<span class="checkmark2"></span>
</label>
<label class="container3" style="float:right;">
<input  class="option3" onclick="comment(36)"type="radio" name="q11" value="3">
<span class="checkmark3"></span>
</label>
<br>
<br>
<p id="comment11"></p>
</div>
</div>
<!-- 13-------------------------------------------------------------------------------------------------- -->
<div id="q13" class="question">
There is a higher power affecting the world around us. <br>
<!-- options container for anchoring the div to the bottom of div-question -->
<div class="options">
<p class="agree">Agree   </p> 
Neutral
<p class="disagree">Disagree</p>
<br>
<label class="container1" style="float:left;">
<input class="option1" onclick="comment(37)" type="radio" name="q12" value="1">
<span class="checkmark1"></span>
</label>
<label class="container2">
<input  class="option2" onclick="comment(38)"type="radio" name="q12" value="2">
<span class="checkmark2"></span>
</label>
<label class="container3" style="float:right;">
<input  class="option3" onclick="comment(39)"type="radio" name="q12" value="3">
<span class="checkmark3"></span>
</label>
<br>
<br>
<p id="comment12"></p>
</div>
</div>
<!-- 14-------------------------------------------------------------------------------------------------- -->
<div id="q14" class="question">
I am a honest person. <br>
<!-- options container for anchoring the div to the bottom of div-question -->
<div class="options">
<p class="agree">Agree   </p> 
Neutral
<p class="disagree">Disagree</p>
<br>
<label class="container1" style="float:left;">
<input class="option1" onclick="comment(40)" type="radio" name="q13" value="1">
<span class="checkmark1"></span>
</label>
<label class="container2">
<input  class="option2" onclick="comment(41)"type="radio" name="q13" value="2">
<span class="checkmark2"></span>
</label>
<label class="container3" style="float:right;">
<input  class="option3" onclick="comment(42)"type="radio" name="q13" value="3">
<span class="checkmark3"></span>
</label>
<br>
<br>
<p id="comment13"></p>
<p id="comment14"></p>   

</div>
</div>
<input class="buttonsubmit" id="submit" type="submit">

</form>
<div>
<!-- <p id="comment"></p> --->
</div>
<img id="forward" src="arrow2.png"  onClick="changeQFor()" onmouseover= "changeArrow1();" onmouseout= "changeArrow2();">
</div>
<img id="tree" src="tree1.png">
   
    <!-- image -->
    </body>
    
    
</html>